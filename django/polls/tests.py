from django.test import TestCase, Client
from http import HTTPStatus

from polls.models import *

class UserTestCase(TestCase):

    def setUp(self):
        self.c = Client()

    def test_is_ok_page_login(self):
        response = self.c.get('')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_is_ok_page_register(self):
        response = self.c.get('/detail')
        self.assertEqual(response.status_code, HTTPStatus.OK)