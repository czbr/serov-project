# Generated by Django 3.1.6 on 2021-07-20 10:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='We',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='Name', max_length=200)),
                ('surname', models.CharField(default='Surname', max_length=200)),
                ('Task', models.CharField(default='Task', max_length=200)),
            ],
        ),
    ]
