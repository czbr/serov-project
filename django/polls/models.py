from django.db import models


class World(models.Model):
    desc = models.CharField(max_length=200, default='Test')
    name = models.CharField(max_length=200, default='Test')

class We(models.Model):
    name = models.CharField(max_length=200, default='Name')
    surname = models.CharField(max_length=200, default='Surname')
    Task =  models.CharField(max_length=200, default='Task')
