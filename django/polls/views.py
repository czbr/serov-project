from django.http import HttpResponse, JsonResponse
import json
from django.template import loader

from .models import *


def index(request):
    latest_question_list = (World.objects.all().values('name', 'desc'))
    data = json.dumps(list(latest_question_list))
    return JsonResponse(data, safe=False)

def Detail(request):
    
    data_list = We.objects.order_by('id')
    template = loader.get_template('index.html')
    context = {
        'creators': data_list,
    }
    return HttpResponse(template.render(context, request))