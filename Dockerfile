FROM python:3.9.6
ENV PYTHONUNBUFFERED=1
WORKDIR /django
COPY requirements.txt /django/
RUN pip install -r requirements.txt
COPY . /django/

# При необходимости сделать миграцию необходимо в окружении выполнить команды ниже
# python3 django/manage.py makemigrations
# python3 django/manage.py migrate
# python3 django/manage.py createsuperuser
